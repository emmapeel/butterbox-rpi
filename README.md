# Butter Box on Raspberry Pi

Turn a Raspberry Pi into a [Butter Box](https://likebutter.app/box).  A Butter Box contains:

* A user friendly homepage
* The Butter App Store & the Butter App for accessing it
* Encrypted Matrix Chat accessible via Keanu Weblite

## Creating a ButterBox from an Image (recommended; see caveats)

You can use the [Raspberry Pi Imager](https://www.raspberrypi.com/software/) to write a ButterBox image to your own SD card and drop it into your RPi.  When you boot it up, you'll have a fully-functioning ButterBox.

ButterBox images are currently available in [this dropbox folder](https://www.dropbox.com/sh/y6ei9l7ixrrw04q/AAAgRsH-Ird619ioxEmeibNta?dl=0).  Files are named `bbb-[primary language]-[commit used to provision].img`.

### Caveats

This image comes preloaded with the public secrets.  Don't use the image method to install if you're in an environment where someone may want to snoop on what you're doing.  Do use it if you're going to test the ButterBox or plan to use it for non-sensitive communications.  Resolving [this issue](https://gitlab.com/likebutter/butterbox-rpi/-/issues/29) will make image-based installs appropriate for sensitive use cases.

## Creating a ButterBox from Scratch

### Requirements

* Raspberry Pi
	* We usually build on a Raspberry Pi 4, but have tested images on:
		* Raspberry Pi 3B
		* Raspberry Pi Zero W
		* Raspberry Pi Zero W 2
	* Note, you may have trouble using a Zero W (2) to build the matrix server.
* A separate machine for imaging a Raspberry Pi Micro SD card, "your
  computer".
* Internet connection for the raspberry pi via ethernet (preferred) or WiFi
  connection for the Raspberry Pi.  This is only needed for setup.
* Access to
  [the butter box UI repository](https://gitlab.com/likebutter/butter-box-ui)
  repo.

### Using the `buttermeup.sh` script

1. Image the RPi.
	* Using Raspberry Pi Imager, select the Raspberry Pi OS Lite (32-bit)
	  image.
	* Using advanced settings (Ctrl+Shift+x):
		* Set the hostname to "butterbox"
		* Enable SSH, set a password suitable for your scenario; test units use
		  "butter".
		* Configure wifi country to "US".  If you don't want the RPi to
		  automatically connect to your wireless network (i.e. you plan to use
		  ethernet), a dummy SSID/password will do (e.g. SSID: Foo; Password:
		  bar).
	* Write the image to the SD Card.
2. Update the operating system and reboot.
	* `sudo apt-get update && sudo apt-get full-upgrade -y && sudo apt-get
	  install git -y && sudo reboot`
3. Copy this repository onto the RPi.
	* If this repository is already on your computer, `cd` into it then run
	  `rsync -r ./ pi@butterbox.local:/tmp/butter-setup` to copy it to the RPi.
	* Or, if this repository is available remotely, SSH into the RPi and clone
	  the repository `git clone
	  https://gitlab.com/guardianproject/wind-offline-fdroid-repo-on-rpi.git
	  /tmp/butter-setup` (this may require credentials).
4. Run the setup script on the RPi via SSH.
	* `sudo bash /tmp/butter-setup/scripts/buttermeup.sh` should do the trick
	* If you find yourself having a flakey connection to the RPi, you can use `screen` e.g. `sudo apt-get install screen -y && screen sudo bash /tmp/butter-setup/scripts/buttermeup.sh`
	* You can specify a language and SSID/domain you'd like to use by using flags:
		* `-l` sets language.  `en` is the default, but `es` is also supported.  This affects the default captive portal and homepage the user is served.  When `es` is chosen, the SSID and default hostname will be comolamantequilla[.lan]. 
	* You can specify the `-c` flag to have the script automatically copy an image of the fresh install to the `/dev/sda` device.  The file will be named bbb.img (butterbox backup).
		* This device needs to be larger than the disk you've provisioned the RPI onto since it will first be copied in full, then shrunk to eliminate empty space.
		* If you want to use the .img on a Mac, you can format a USB drive as ExFAT, which will be both writable to the RPi and readable on your Mac.
		* This takes about an hour to copy (then shrink) a 32GB image.
5. Install may take an hour or so depending on network and RPi performance.  The RPi will automatically restart, and should broadcast the network "butterbox" or "comolamantequilla" when it comes back up.  If that fails, manually power cycle the RPi.

## Verifying your Butterbox is working properly

A functioning butterbox should be able to perform the following use cases.
These make good QA/Integration Tests for updates to the script, underlying
dependencies, or hardware.

Ideally, these tests are run with a factory reset android device.  If that's
not convenient, an android device without the butter app can also suffice.

### As a User, I'd like to install new apps (without any Internet connection to my device or the butterbox).

1. Disconnect the internet from the butterbox (if connected via Ethernet).
2. Turn on or power cycle the butterbox.
3. The butterbox should be broadcasting the SSID "butterbox".
4. Connect the device to the butterbox's wifi network, which should not
   require a password.
5. The captive portal should show up when the device connects or when the
   user attempts to load a website in the browser (behavior depends on the
   device).
6. Click "Continue" on the captive portal splash page.
7. The device should be redirected to "http://butterbox.lan" which should
   display a yellow page including a link to download the butter app.
	* Note: On some devices, the browser used to display captive portals may
	  not allow #8 or #9.  In this case, open http://butterbox.lan in an
	  ordinary browser.
8. The language should be toggleable between English and Spanish.
9. Clicking "Download Android App" should download the app.
10. The app package should be installable (but may require users to approve
    a new source, e.g. their browser).
11. When the Butter app is opened, a list of apps should be presented.
    Click an app not yet installed on the android device and install it.
12. The app should install (this may also require the user to give install
    permissions to Butter) and open without error.

### As a user, I'd like to install new apps (if my device sporadically has internet). 

1. After installing Butter per the use case above, connect to a wifi network
   that has Internet access.
2. Disable the last repository in the Butter Settings > Repositories menu.
   Ideally, this step would not be required, but F-Droid client seems to try
   to download from the first repository where it saw an app (in this case
   butterbox.lan) unless that repository is disabled.
3. Refresh the categories or latest menu in Butter.
4. Download and install an app not yet installed.

### As a user, I'd like to be able to use an ES-localized Butterbox 

0. Create a butterbox using the `-l es` flag & set the test phone's language
   to ES.
1. The butterbox should broadcast the network "comolamantequilla", which
   should not require a password.
2. The captive portal should appear in ES.
3. Clicking continuar should bring the user to http://comolamantequilla.lan,
   which should be rendered in ES.
4. The app should be downloadable and when downloaded, should appear on the
   homepage as "Mantequilla"
5. Application names and descriptions should appear in ES within Mantequilla
   and when installed.

## Onboard IPFS

Butter Box comes with a builtin [IPFS](https://ipfs.tech/) node and a web UI for uploading files to IPFS.  When a user uploads a file, the the local node will pin the file and return the hash of the newly uploaded file to the user.

### Downloading and viewing uploaded files

You can use the IPFS CLI to list the files pinned by the Butter Box.  SSH into the box and use `ipfs pin ls --type=recursive` to see the list of CIDs.  Alternatively, you can use `$ curl -X POST "http://butterbox.lan:5001/api/v0/pin/ls?type=recursive"` from your local machine connected to a butter box to get a JSON formatted list of pins.

To download files, you can use your local IPFS client.  Running `ipfs get <CID>` will download the file to your computer, albeit without an extension.  If you know the filename and extension that should be applied to that CID, great!  If you don't, your browser will often recognize the filetype.  Drag the file into your browser to preview and/or save the file with an appropriate extension.  Alternatively, you can use `➜ curl -X POST "http://butterbox.lan:5001/api/v0/get?arg=<CID>" --output <filename.zip>` to get a ZIP containing the CID's file.

### Making pinned files available on the IPFS mainnet

If (or when) the Butter Box is connected to the Internet, it will make the file available on the main IPFS network.  If you want these CIDs to remain accessible after the Butter Box is disconnected and returned to its offline home, you'll need to ask a pinning service to pin those CIDs.

## Localization

Localization is core to the use case for Butter Box.  Because this project
installs sources from all over, there are many different methods of
localization with varying language support.  The table below tracks the
availability of languages in Butter Box's various functions:

|Function                      | `en` | `es` | `zh` | `bo` | Notes |
|------------------------------|------|------|------|------|-------|
|This document: README.md  |✅|✅|||
|Documentation: USER GUIDE.md  |✅|✅||| 
|[butter-box-ui](https://gitlab.com/likebutter/butter-box-ui/) |✅|✅||| User-selectable.  Defaults to language Butter Box was provisioned for.  To be supported via weblate.
|[Butter App](https://gitlab.com/likebutter/butterapp)         |✅|✅|✅|✅| Automatically displayed based on Android language.
|[Apps within Butter App](https://likebutter.app/apps/)        |✅|🟡|🟡|🟡| App-specific support.
|RaspAP Admin Interface        |✅|✅|✅|❌| Served based on browser's preferred language. See [docs](https://docs.raspap.com/translations/).
|Keanu-Weblite chat            |✅|✅|✅|✅| Served based on browser's preferred language.

✅ = supported

🟡 = incompletely supported (e.g. on a per-app basis)

❌ = not supported by this project

Note: We plan to support more languages in the future including Uighur
(`ug`), Russian (`ru`), and Ukrainian (`uk`) once pipelines in place for the
functions above.
