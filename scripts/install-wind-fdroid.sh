set -e

# Pull down a copy of the wind repository and unzip it into the expected path
curl https://guardianproject-wind.s3.amazonaws.com/repo.zip --output /tmp/fdroid-repo.zip
unzip /tmp/fdroid-repo.zip -d /tmp/fdroid-repo
# Replace the S3 hostname with our LAN address
old_host="https://guardianproject-wind.s3.amazonaws.com"
new_host="http://$butter_name.lan";
sed -i "s~$old_host~$new_host~g" /tmp/fdroid-repo/guardianproject-wind.s3.amazonaws.com/fdroid/repo/index.html
# remove the QR code that points to the cloud address
rm /tmp/fdroid-repo/guardianproject-wind.s3.amazonaws.com/fdroid/repo/index.png
mv /tmp/fdroid-repo/guardianproject-wind.s3.amazonaws.com/fdroid/ /var/www/html