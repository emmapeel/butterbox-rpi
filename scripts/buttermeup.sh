# Turn a freshly imaged RPi into a butterbox.  Execute with sudo.
# copy this whole directory to the newly imaged RPi with
# rsync -r ./ pi@butterbox.local:/tmp/butter-setup
# Then ssh into the box and run this script
# sudo -E bash /tmp/butter-setup/scripts/buttermeup.sh
# If your terminal connection to the pi is flakey, try:
# sudo apt-get install screen -y && screen sudo -E bash /tmp/butter-setup/scripts/buttermeup.sh

# If any command in this script fails, exit.
set -e

language="en"
copy_image_to_sda=false

while getopts ":l:c" flag
do 
	case "${flag}" in
		l) language=${OPTARG};;
		c) copy_image_to_sda=true;;
	esac
done

case $language in
	en) name="butterbox";;
	es) name="comolamantequilla";;
	# These SSIDs are direct from butter-box-ui's site_name field ("butter" in en)
	zh_Hans) name="黄油";;
esac

echo "Setting up butterbox with";
echo "    SSID:             $name";
echo "    Default language: $language";
echo "    Copying to sda?   $copy_image_to_sda";
echo "";
export butter_language=$language
export butter_name=$name

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
mkdir -p /var/log/butter

echo "checking that we can access gitlab with a forwarded SSH key"
ssh -o StrictHostKeyChecking=no git@gitlab.com &>> /var/log/butter/install.txt

echo "Installing RaspAP"
bash $SCRIPT_DIR/install-raspap.sh &> /var/log/butter/install.txt

echo "Installing Butter Site"
bash $SCRIPT_DIR/install-butter-site.sh &> /var/log/butter/install.txt

echo "Installing IPFS drop"
bash $SCRIPT_DIR/install-ipfsdrop.sh &> /var/log/butter/install.txt

# Skip nodogsplash which does not pop a portal when there's no upstream connectivity.
echo "Skipping NoDogSplash"
# echo "Installing NoDogSplash"
# bash $SCRIPT_DIR/install-nodogsplash.sh &> /var/log/butter/install.txt

echo "Installing Wind F-Droid"
bash $SCRIPT_DIR/install-wind-fdroid.sh &> /var/log/butter/install.txt

echo "Installing AnyNews"
bash $SCRIPT_DIR/install-anynews.sh &> /var/log/butter/install.txt

echo "Installing Matrix Chat"
bash $SCRIPT_DIR/install-chat.sh &> /var/log/butter/install.txt

echo "Installing rpi-clone"
bash $SCRIPT_DIR/install-rpi-clone.sh &> /var/log/butter/install.txt

echo "Installing AP-optimized wifi firmware"
bash $SCRIPT_DIR/install-ap-optimized-firmware.sh &> /var/log/butter/install.txt

echo "Removing wifi creds from wpa_supplicant"
bash $SCRIPT_DIR/remove-wifi-creds.sh &> /var/log/butter/install.txt

if [ "$copy_image_to_sda" = true ] ; then
	echo "Imaging this RPi Install"
	bash $SCRIPT_DIR/make-img.sh &> /var/log/butter/install.txt
fi

reboot