set -e

git clone https://github.com/billw2/rpi-clone.git
cd rpi-clone
cp rpi-clone rpi-clone-setup /usr/local/sbin
rpi-clone-setup -t ${butter_name}