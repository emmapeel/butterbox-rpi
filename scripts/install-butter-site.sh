set -e

git clone git@gitlab.com:likebutter/butter-box-ui.git /tmp/butter-site

# Put the butter site at the webroot
# TODO: Build this on a faster machine than an RPi where this can take 10s of minutes
apt-get install ruby-full -y
gem install jekyll
gem install bundler
cd /tmp/butter-site
bundle install

# Remove the primary language from it's spot in the config and add it in the first spot
grep -v "'$butter_language'," /tmp/butter-setup/configs/butter-box-ui-_config.yml > /tmp/butter-setup/configs/_config.yml
sed -i "s/PRIMARY_LANGUAGE_CODE/$butter_language/g" /tmp/butter-setup/configs/_config.yml
cp /tmp/butter-setup/configs/_config.yml /tmp/butter-site

# use the offline, chat-supporting deployment profile for butter box
cp _data/deployments/butterbox/deployment.yml _data/deployment.yml
# Make sure the site can link to the matrix server by name
# This is not the same as localization.  Even on a spanish-first box, the english page
# must refer to #someroom:comolamantequilla.lan.
sed -i "s/REPLACEME/$butter_name/g" _data/deployment.yml
bundle exec jekyll build

# move the whole site into the webroot
cp -r /tmp/butter-site/_site/* /var/www/html