# Makes an .img of this install and puts it on a USB drive, if inserted.
# Then shrinks that down so it can be copied over
# bbb = butter box backup

apt-get install pv
mkdir -p /media/bbb

# Assume sda since this is a fresh install with a new disk
# Note: to have this disk readable on mac and able to handle
# a 32GB image, ExFAT works well.
sudo mount /dev/sda1 /media/bbb/
dd if=/dev/mmcblk0 bs=512 | pv | dd of=/media/bbb/bbb.img bs=512

# Install PiShrink so we don't end up with a 32GB image
wget https://raw.githubusercontent.com/Drewsif/PiShrink/master/pishrink.sh
chmod +x pishrink.sh
mv pishrink.sh /usr/local/bin

# Shrink the image
pishrink.sh /media/bbb/bbb.img