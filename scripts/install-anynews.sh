set -e

# Install anynews republisher and fetch the first set of news
# TODO: remove -b butter when merged to main
sudo -u pi git clone -b butter https://gitlab.com/guardianproject/anynews/AnyNews-Republisher.git /home/pi/Software/guardianproject/anynews
cd /home/pi/Software/guardianproject/anynews
apt-get install composer php-xml php7.4-sqlite3 php7.4-curl -y
sudo -u pi composer install
bash ./retrieve.sh verbose
bash ./tidy.sh
