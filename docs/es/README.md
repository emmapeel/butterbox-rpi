# Caja de Mantequilla en Raspberry Pi

Convierte una Raspberry Pi en una [Caja
Mantequilla](https://likebutter.app/box). Una caja de mantequilla contiene:

* Una página de inicio fácil de usar
* La tienda de aplicaciones Mantequilla & la aplicación Butter para acceder
  a ella
* Chat de matrix cifrado accesible a través de Keanu Weblite

## Creación de una Caja Mantequilla a partir de una imagen (recomendado; consulte las advertencias)

Puedes usar [Raspberry Pi Imager](https://www.raspberrypi.com/software/)
para escribir una imagen de ButterBox en tu propia tarjeta SD y colocarla en
tu RPi. Cuando lo inicies, tendrás una Caja Mantequilla en pleno
funcionamiento.

Las imágenes de la Caja Mantequilla están actualmente disponibles en [esta
carpeta de Dropbox]
(https://www.dropbox.com/sh/y6ei9l7ixrrw04q/AAAgRsH-Ird619ioxEmeibNta?dl=0).
Los archivos se denominan `bbb-[idioma principal]-[commit used to
provision].img`.

### Advertencias

Esta imagen viene precargada con los secretos públicos. No utilices el
método de imagen para instalar si te encuentra en un entorno en el que
alguien puede querer curiosear en lo que estás haciendo. Úsalo si vas a
probar la caja Mantequilla o planeas usarla para comunicaciones no
confidenciales. Resolver [este
problema](https://gitlab.com/likebutter/butterbox-rpi/-/issues/29) hará que
las instalaciones basadas en imágenes sean apropiadas para casos de uso
delicado.

## Crear una caja Mantequilla desde cero

### Requerimientos

* Raspberry Pi
	* Por lo general, construimos sobre una Raspberry Pi 4, pero hemos probado
	  imágenes en:
		* Raspberry Pi 3B
		* Raspberry Pi Zero W
		* Raspberry Pi Zero W 2
	* Ten en cuenta que puedes tener problemas para usar un Zero W (2) para
	  construir el servidor de matriz.
* Una computadora para crear imágenes de una tarjeta Raspberry Pi Micro SD.
* Conexión a Internet para la raspberry pi a través de ethernet (preferido)
  o conexión WiFi para la Raspberry Pi. Esto solo es necesario para la
  configuración.
* Acceda al repositorio [del repositorio de interfaz de usuario de butter
  box] (https://gitlab.com/likebutter/butter-box-ui).

### Usando el script `buttermeup.sh`

1. Crea una imagen del RPi.
	* Con Raspberry Pi Imager, selecciona la imagen Raspberry Pi OS Lite (32
	  bits).
	* Usando la configuración avanzada (Ctrl+Shift+x):
		* Establezca el nombre de host en "butterbox"
		* Habilite SSH, establezca una contraseña adecuada para su escenario; las
		  unidades de prueba usan "mantequilla".
		* Configura el país wifi en "EE. UU.". Si no deseas que el RPi se conecte
		  automáticamente a tu red inalámbrica (es decir, planeas usar Ethernet),
		  un SSID/contraseña ficticio servirá (por ejemplo, SSID: Foo; Contraseña:
		  bar).
	* Graba la imagen en la tarjeta SD.
2. Actualiza el sistema operativo y reinicia.
	* `sudo apt-get update && sudo apt-get full-upgrade -y && sudo apt-get
	  install git -y && sudo reboot`
3. Copia este repositorio en el RPi.
	* Si este repositorio ya está en tu computadora, has `cd` en él y luego
	  ejecute `rsync -r ./ pi@butterbox.local:/tmp/butter-setup` para copiarlo
	  al RPi.
	* O, si este repositorio está disponible de forma remota, SSH en el RPi y
	  clona el repositorio `git clone
	  https://gitlab.com/guardianproject/wind-offline-fdroid-repo-on-rpi.git
	  /tmp/butter-setup` (esto puede requerir credenciales).
4. Ejecuta el script de configuración en el RPi a través de SSH.
	* Ejecuta `sudo bash /tmp/butter-setup/scripts/buttermeup.sh`para
	  conseguirlo
	* Si tienes una conexión inestable con el RPi, puedes usar `screen`,
	  p. `sudo apt-get install screen -y && screen sudo bash
	  /tmp/butter-setup/scripts/buttermeup.sh`
	* Puedes especificar un idioma y SSID/dominio que te gustaría usar usando
	  banderas:
		* `-l` establece el idioma. `en` es el valor predeterminado, pero `es`
		  también es compatible. Esto afecta el portal cautivo predeterminado y la
		  página de inicio a la que se sirve el usuario. Cuando se elige `es`, el
		  SSID y el nombre de host predeterminado serán comolamantequilla[.lan].
	* Puedes especificar el indicador `-c` para que el script copie
	  automáticamente una imagen de la instalación nueva en el dispositivo
	  `/dev/sda`. El archivo se llamará bbb.img (copia de seguridad de
	  butterbox).
		* Este dispositivo debe ser más grande que el disco en el que ha
		  aprovisionado el RPI, ya que primero se copiará por completo y luego se
		  reducirá para eliminar el espacio vacío.
		* Si deseas utilizar .img en una Mac, puedes formatear una unidad USB como
		  ExFAT, que se podrá escribir en el RPi y se podrá leer en tu Mac.
		* Se tarda aproximadamente una hora en copiar (luego reducir) una imagen
		  de 32 GB.
5. La instalación puede demorar aproximadamente una hora, según la red y el
   rendimiento de RPi. El RPi se reiniciará automáticamente y debería
   transmitir la red "butterbox" o "comolamantequilla" cuando vuelva a
   funcionar. Si eso falla, reinicia manualmente el RPi.

## Verificando que tu Butterbox esté funcionando correctamente

Una caja Mantequilla en funcionamiento debería poder realizar los siguientes
casos de uso. Estas son buenas pruebas de control de calidad/integración
para actualizaciones del script, dependencias subyacentes o hardware.

Idealmente, estas pruebas se ejecutan con un dispositivo Android
restablecido de fábrica. Si eso no es conveniente, un dispositivo Android
sin la aplicación Mantequilla también puede ser suficiente.

### Como usuario, me gustaría instalar nuevas aplicaciones (sin ninguna conexión a Internet a mi dispositivo o a la caja Mantequilla).

1. Desconecta Internet de la caja Mantequilla (si estás conectado a través
   de Ethernet).
2. Enciende o apaga y enciende la caja Mantequilla.
3. La caja Mantequilla debería transmitir el SSID "butterbox".
4. Conecta el dispositivo a la red wifi de la caja Mantequilla, que no
   debería requerir una contraseña.
5. El portal cautivo debería aparecer cuando el dispositivo se conecta o
   cuando el usuario intenta cargar un sitio web en el navegador (el
   comportamiento depende del dispositivo).
6. Has clic en "Continuar" en la página de bienvenida del portal cautivo.
7. El dispositivo debe redirigirse a "http://butterbox.lan", que debe
   mostrar una página amarilla que incluye un enlace para descargar la
   aplicación de Mantequilla.
	* Nota: En algunos dispositivos, es posible que el navegador utilizado para
	  mostrar portales cautivos no permita el #8 o el #9. En este caso, abre
	  http://butterbox.lan en un navegador normal.
8. El idioma debe poder alternarse entre inglés y español.
9. Al hacer clic en "Descargar aplicación de Android" deberías descargar la
   aplicación.
10. El paquete de la aplicación debe poder instalarse (pero puede requerir
    que los usuarios aprueben una nueva fuente, por ejemplo, tu navegador).
11. Cuando se abre la aplicación Butter, se debe presentar una lista de
    aplicaciones. Has clic en una aplicación que aún no esté instalada en el
    dispositivo Android e instálala.
12. La aplicación debería instalarse (esto también puede requerir que el
    usuario otorgue permisos de instalación a Mantequilla) y abrirse sin
    errores.

### Como usuario, me gustaría instalar nuevas aplicaciones (si mi dispositivo tiene internet esporádicamente). 

1. Después de instalar Mantequilla según el caso de uso anterior, conéctate
   una red wifi que tenga acceso a Internet.
2. Deshabilita el último repositorio en el menú Ajustes de mantequilla >
   Repositorios. Idealmente, este paso no sería necesario, pero el cliente
   F-Droid parece intentar descargar desde el primer repositorio donde vio
   una aplicación (en este caso, butterbox.lan) a menos que ese repositorio
   esté deshabilitado.
3. Actualiza las categorías o el menú más reciente en Mantequilla.
4. Descarga e instale una aplicación que aún no se haya instalado.

### Como usuario, me gustaría poder usar una caja Mantequilla localizada en ES 

0. Crea una caja Mantequilla usando el indicador `-l es` y establece el
   idioma del teléfono de prueba en ES.
1. La caja Mantequilla debe transmitir la red "comolamantequilla", la cual
   no debe requerir contraseña.
2. El portal cautivo se debería aparecer en ES.
3. Hacer clic en continuar debe llevar al usuario a
   http://comolamantequilla.lan, que debe estar en ES.
4. El app debe poder descargarse y cuando se descarga, la aplicación debe
   aparecer en la página de inicio como "Mantequilla"
5. Los nombres de la aplicación y las descripciónes deben aparecer en ES
   dentro de la Mantequilla y cuando se instala.

## Integrando IPFS

Butter Box viene con un nodo [IPFS](https://ipfs.tech/) integrado y una interfaz de usuario web
para subir archivos a IPFS. Cuando un usuario carga un archivo, el nodo local
anclará el archivo y devolverá el hash del archivo recién cargado al
usuario.

### Descarga y visualización de archivos cargados

Puedes usar la CLI de IPFS para enumerar los archivos fijados por Butter Box. SSH
en el cuadro y usa `ipfs pin ls --type=recursive` para ver la lista de
CID. Alternativamente, puedes usar `$ curl -X POST
"http://butterbox.lan:5001/api/v0/pin/ls?type=recursive"` de tu computadora local 
 conectada a una caja de mantequilla para obtener una lista de pines con formato JSON.

Para descargar archivos, puedes usar su cliente IPFS local. Ejecutar `ipfs get <CID>` descargará el archivo a tu computadora, aunque sin extensión. Si conoces el nombre de archivo y la extensión que debe aplicarse a ese CID, ¡excelente! Si no, tu navegador a menudo reconocerá el tipo de archivo. Arrastre el archivo a tu navegador para obtener una vista previa y/o guardar el archivo con una extensión adecuada. Alternativamente, puedes usar `➜ curl -X POST "http://butterbox.lan:5001/api/v0/get?arg=<CID>" --output <filename.zip>` para obtener un ZIP que conten los CID. archivo.

### Hacer que los archivos anclados estén disponibles en la red principal de IPFS

Si (o cuando) la Butter Box está conectada al Internet, hará 
archivo disponible en la red IPFS principal. Si deseas que estos CID permanezcan
accesible después de que Butter Box se desconecte, deberás solicitar un servicio de fijación para fijar esos CID.

## Localización

La localización es fundamental para el caso de uso de Butter Box. Debido a
que este proyecto instala fuentes de todas partes, existen muchos métodos
diferentes de localización con compatibilidad con diferentes idiomas. La
siguiente tabla rastrea la disponibilidad de idiomas en las diversas
funciones de Butter Box:

|Function                      | `en` | `es` | `zh` | `bo` | Notes |
|------------------------------|------|------|------|------|-------|
|This document: README.md  |✅|✅|||
|Documentation: USER GUIDE.md  |✅|✅||| 
|[butter-box-ui](https://gitlab.com/likebutter/butter-box-ui/) |✅|✅||| User-selectable.  Defaults to language Butter Box was provisioned for.  To be supported via weblate.
|[Butter App](https://gitlab.com/likebutter/butterapp)         |✅|✅|✅|✅| Automatically displayed based on Android language.
|[Apps within Butter App](https://likebutter.app/apps/)        |✅|🟡|🟡|🟡| App-specific support.
|RaspAP Admin Interface        |✅|✅|✅|❌| Served based on browser's preferred language. See [docs](https://docs.raspap.com/translations/).
|Keanu-Weblite chat            |✅|✅|✅|✅| Served based on browser's preferred language.

✅ = soportada

🟡 == soporte incompleto (por ejemplo, por aplicación)

❌ = no soportado en este proyecto

Nota: planeamos admitir más idiomas en el futuro, incluidos el uigur (`ug`),
el ruso (`ru`) y el ucraniano (`uk`), una vez que se implementen las
canalizaciones para las funciones anteriores.
