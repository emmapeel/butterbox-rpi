# Como usar la Caja Mantequilla

## ¿Qué hace?

Butter Box hace que la vida sin Internet sea un poco más fácil al brindar
acceso a las aplicaciones. Simplemente conéctate al wifi de Butter Box,
luego descarga la tienda de aplicaciones Butter y elije las aplicaciones que
deses instalar.

## Configuración de una caja de mantequilla

Simplemente conéctalo usando el adaptador de corriente incluido o una fuente
de alimentación micro USB adecuada. En unos segundos, verás que está
transmitiendo la red wifi "butterbox".

![Conecta la Caja Mantequilla](images/plugged-in-to-outlet.jpg)

![Conecta la caja Mantequilla a un panel
solar](images/plugged-in-to-solar.jpg)

La caja Mantequilla funciona sin conexión a Internet y viene precargada con
todo lo que necesitas.

## Usando la caja Mantequilla

Con un dispositivo Android, conéctate a la red wifi de Butterbox. Para
asegurarte de que tu dispositivo busque información de la caja Mantequilla
en lugar de Internet, apagua la conexión celular de tu teléfono.

![Conectarse a la red Wifi de Mantequilla](images/en/wifi-network.jpg)

Si tu dispositivo te muestra la pantalla de bienvenida del portal cautivo,
¡excelente! Si no, visita `http://butterbox.lan`

![Conectarse a la red de Wifi de
Mantequilla](images/en/qr/butterbox.lan.png)

Desde la página de inicio de Mantequilla, has clic en "Descargar aplicación
de Android" y luego has clic en "Descargar aplicación de Android" nuevamente
desde el modal. Esto descargará un archivo llamado "butter.apk" a tu
dispositivo.

![Descargar Mantequilla.](images/en/download.jpg)

Abre "mantequilla.apk". Es posible que debas permitir la instalación desde
fuentes desconocidas. Esto instalará la aplicación Butter, que ahora puedes
abrir.

![La aplicación de mantequilla en la página de inicio](images/en/app.jpg)

Después de que la aplicación Mantequilla cargue el repositorio, verás una
lista de las aplicaciones disponibles. Has clic en cualquier aplicación para
leer más y luego en "Instalar" para instalar la aplicación en tu
dispositivo.

![Lista de aplicaciones de la tienda de
Mantequilla](images/en/butter-app-store.jpg)

## Usar Mantequilla con acceso a internet

Con la aplicación Butter, puedes instalar aplicaciones sin conexión a
Internet siempre que estés conectado al wifi de Butter Box. Cuando tengas
una conexión a Internet, también puedes usar la aplicacion Mantequilla sin
conectarte a la caja Mantequilla.

Si tienes una conexión a Internet, puedes incluso descargar la aplicacion
Mantequilla sin acceso a una caja Mantequilla. Simplemente visite [el sitio
web de Mantequilla] (https://likebutter.app/).
