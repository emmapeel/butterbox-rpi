# How to use a Butter Box

## What does it do?

Butter Box makes life without internet a bit easier by providing access to apps.  Just connect to the Butter Box's wifi, then download the Butter app store and choose the apps you want to install.

## Setting up a Butter Box

Simply plug it in using the included power adapter or a suitable micro USB power source.  In a few seconds, you'll see it's broadcasting the wifi network "butterbox".

![Plug the butter box into an outlet](images/plugged-in-to-outlet.jpg)

![Plug the butter box into a solar power supply](images/plugged-in-to-solar.jpg)

The Butter Box works without an Internet connection and comes pre-loaded with everything it needs.

## Using the Butter Box

Using an Android device, connect to the butterbox wifi network.  To make sure your device seeks information from the Butter Box instead of the broader internet, turn off your phone's cellular connection.

![Connect to the butterbox wifi network](images/en/wifi-network.jpg)

If your device shows you the captive portal splash screen, great! If not, visit `http://butterbox.lan`

![Connect to the butterbox wifi network](images/en/qr/butterbox.lan.png)

From the Butter homepage, click "Download Android App" then click "Download Android App" again from the modal.  This will download a file called "butter.apk" to your device.

![Download butter.](images/en/download.jpg)

Open "butter.apk".  You may need to allow installation from unknown sources.  This will install the Butter App, which you can now open.

![The butter app on the homepage](images/en/app.jpg)

After the Butter App loads the repository, you'll see a list of the available apps.  Click any app to read more and then "Install" to install the app to your device.

![The butter app store listing apps](images/en/butter-app-store.jpg)

## Using Butter with an Internet connection

With the Butter app, you can install apps without an Internet connection as long as you're connected to the Butter Box's wifi.  When you do have an Internet connection, you can also use Butter away from the Butter Box.

If you have an Internet connection, you can even download Butter without access to a Butter Box.  Just visit [the Butter website](https://likebutter.app/).